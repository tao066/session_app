class Name < ApplicationRecord
  validates :first_name,
    presence: true

  validates :last_name,
    presence: true

  def self.search(search)
    if search
           Name.where(['first_name LIKE ?', "%#{search}%"])
      .or( Name.where(['last_name LIKE ?' , "%#{search}%"]) )
    else
      Name.all
    end
  end

  def user
    return User.find_by(id: self.user_id)
  end

end
