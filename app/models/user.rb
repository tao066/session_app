class User < ApplicationRecord
  # email の正規表現
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  # コールバックの定義
  before_save { self.email = email.downcase }

  validates :email,
    presence: true,
    length: { maximum: 255 },
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: { case_sensitive: false }

  has_secure_password
    validates :password,
      presence: true,
      length: { minimum: 6 }

  validates :category_id,
    presence: true

  def category
    category = Category.find_by(id: self.category_id)
    return category.name
  end

  def name
    return Name.find_by(user_id: self.id)
  end

  def name_info
    name = self.name
    return  self.name.last_name + " " + self.name.first_name
  end

  def self.search(c_search, t_search, n_search)
    names = Name.search(n_search)
    num = []
    names.each { |name| num.push( name.user_id ) }
    users = n_search ? User.where(id: num) : User.all
    users = c_search ? users.where(['category_id LIKE ?', "%#{c_search}%"]) : users.all
    users = t_search ? users.where(['tag_id LIKE ?',      "%#{t_search}%"]) : users.all
    return users
  end

  def tag
    tag = Tag.find_by(id: self.tag_id)
    if tag.nil? == false
      return tag.name
    else
      return ""
    end
  end

end
