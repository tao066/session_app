class Post < ApplicationRecord
  validates :content,
    presence: true,
    length: {maximum: 140}

  validates :room_id,
    presence: true

  validates :user_id,
    presence: true

  def username
    user = User.find_by(id: self.user_id)
    return user.name_info
  end

  def image_name
    user = User.find_by(id: self.user_id)
    return user.image_name
  end

  def simple_time
    return self.created_at.strftime("%Y-%m-%d %H:%M")
  end
end
