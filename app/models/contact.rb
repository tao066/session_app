class Contact < ApplicationRecord
  validates :room_id,
    presence: true

  validates :user_id,
    presence: true

  def roomname
    room = Room.find_by(id: self.room_id)
    return room.name
  end
end
