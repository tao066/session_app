//
//これはマニフェストファイルで、application.jsにコンパイルされます。
//このファイルには、以下に挙げるすべてのファイルが含まれます。
//
//このディレクトリ内のJavaScript / Coffeeファイル、lib / assets / javascripts、
//またはプラグインのvendor / assets / javascriptsディレクトリは、ここでは相対パスを使用して参照できます。
//
//ここに直接コードを追加することはお勧めできませんが、そうした場合、コンパイルされたファイルの一番下に表示されます。
//
//このファイルのJavaScriptコードは、最後のrequire_ *文の後に追加する必要があります。
//
//サポートされるディレクティブの詳細については、
//Sprockets README（https://github.com/rails/sprockets#sprockets-directives）を参照してください。
//
//= require rails-ujs
//= require turbolinks
//= require_tree .
//= require jquery
//= require bootstrap-sprockets

$(function() {

  $('.signin-pricing').click(function() {
    $('#signin-modal').fadeIn();
  });
  $('#close-signin-modal').click(function() {
    $('#signin-modal').fadeOut();
  });

});