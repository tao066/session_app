module ApplicationHelper

  # main_title を返す
  def main_title(page_title = '')
    instead_title = "タイトル"
    if page_title.empty?
      instead_title
    else
      page_title
    end
  end

end
