module SessionsHelper

  # 渡されたユーザーでログインする
  def log_in(user)
    session[:user_id] = user.id
  end

  # 現在ログイン中のユーザーを返す (いる場合)
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  # ユーザーがログインしていればtrue、その他ならfalseを返す
  def logged_in?
    !current_user.nil?
  end

  # 現在のユーザーをログアウトする
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end

  def authenticate_user
    if current_user == nil
      flash.now[:alert] = "ログインが必要です"
      redirect_to("/session")
    end
  end

  def forbid_login_user
    if current_user
      flash.now[:notice] = "既にログインしています"
      redirect_to("/users/#{ current_user.id }")
    end
  end

end
