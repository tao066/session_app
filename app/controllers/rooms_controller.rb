class RoomsController < ApplicationController
  before_action :authenticate_user
  before_action :have_not_contact, only: [:show]
  before_action :room_out

  def index
    @rooms = Contact.where(user_id: current_user.id)
    @rooms = @rooms.paginate(page: params[:page])
  end

  def create
    @user = User.find_by(id: params[:id])
    ans = already_contact?(current_user, @user)
    if ans
      redirect_to "/room/#{ ans }"
    else
      @room = Room.create!(
        name: "#{ current_user.name_info } と #{ @user.name_info } のセッション"
      )
      Contact.create!(user_id: current_user.id, room_id: @room.id)
      Contact.create!(user_id: @user.id,        room_id: @room.id)
      redirect_to "/room/#{ @room.id }"
    end
  end

  def already_contact?(c_user, p_user)
    ans = false
    c_cons = Contact.where(user_id: c_user.id)
    p_cons = Contact.where(user_id: p_user.id)
    c_cons.each do |c_con|
      p_cons.each do |p_con|
        c_con.room_id == p_con.room_id ? ans = c_con.room_id : ans = ans
      end
    end
    return ans
  end

  def have_not_contact
    if Contact.find_by(room_id: params[:id], user_id: current_user.id).nil?
      flash[:alert] = "権限がありません"
      redirect_to "/room"
    end
  end

end
