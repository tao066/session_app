class HomePagesController < ApplicationController
  before_action :forbid_login_user,{only: [:index]}
  before_action :room_out

  def index
    @user = User.new
    @categories = Category.all
  end

  def privacy_policy
  end

end
