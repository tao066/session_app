class UsersController < ApplicationController
  before_action :authenticate_user, {only: [:index, :show]}
  before_action :forbid_login_user, {only: [:create]}
  before_action :room_out

  def index
    @categories = Category.all
    @tags = Tag.all
    @users = User.search(params[:category_search], params[:tag_search], params[:name_search])
    @users = @users.paginate(page: params[:page])
  end

  def show
    @room = Room.new
    @user = User.find_by(id: params[:id])
    @tags = Tag.all
  end

  def create
    @user = User.new(user_params)
    @user.category_id = params[:category_id]
    @user.image_name = params[:category_id] == 1 ? "student.png" : "teacher.png"
    if @user.save
      redirect_to "/users/#{ @user.id }/name/create"
    else
      @categories = Category.all
      flash.now[:alert] = "新規登録に失敗しました。もう一度新規登録ボタンをクリックして、エラーメッセージを基に修正してください。"
      render "home_pages/index"
    end
  end

  def image
    @user = User.find_by(id: params[:id])
    if params[:image]
      @user.image_name = "#{@user.id}.jpg"
      @user.update_attribute(:image_name, "#{@user.id}.jpg")
      image = params[:image]
      File.binwrite("app/assets/images/users/#{@user.image_name}",image.read)
    else
      @user.image_name = params[:category_id] == 1 ? "student.png" : "teacher.png"
    end
    redirect_to "/users/#{@user.id}"
  end

  private
    def user_params
      params.require(:user).permit(
        :email,
        :password,
        :password_confirmation
      )
    end

end
