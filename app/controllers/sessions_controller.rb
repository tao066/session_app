class SessionsController < ApplicationController
  before_action :authenticate_user, {only: [:destroy]}
  before_action :forbid_login_user, {only: [:new, :create]}
  before_action :room_out

  def new
  end

  def create
    @user = User.find_by(email: params[:email])
    if @user && @user.authenticate(params[:password])
      if @user.name
        log_in(@user)
        flash[:notice] = "ようこそ #{ @user.name.last_name + " " + @user.name.first_name } さん"
        redirect_to "/users/#{ @user.id }"
      else
        redirect_to "/users/#{ @user.id }/name/create"
      end
    else
      flash.now[:alert] = "メールアドレスまたはパスワードが間違っています"
      @email = params[:email]
      render 'sessions/new'
    end
  end

  def destroy
    log_out
    flash[:notice] = "ログアウトしました"
    redirect_to root_url
  end
end
