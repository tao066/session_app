class PostsController < ApplicationController
  before_action :authenticate_user
  before_action :have_not_contact, only: [:index]
  before_action :current_room, only: [:new, :create]

  def index
    @room = Room.find_by(id: params[:id])
    @posts = Post.where(room_id: params[:id]).order(created_at: :desc)
    room_in(@room)
  end

  def new
    @posts = Post.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @posts = Post.where(room_id: current_room.id).order(created_at: :desc)
    @post = Post.new(post_content)
    @post.user_id = current_user.id
    @post.room_id = current_room.id
    respond_to do |format|
      if @post.save
        format.html
        format.js
      else
        format.js {render :new}
      end
    end
  end

  def post_content
    params.require(:post).permit(:content)
  end

  def have_not_contact
    if Contact.find_by(room_id: params[:id], user_id: current_user.id).nil?
      flash[:alert] = "権限がありません"
      redirect_to "/room"
    end
  end

end
