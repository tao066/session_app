class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  def room_in(room)
    session[:room_id] = room.id
  end

  def current_room
    @room ||= Room.find_by(id: session[:room_id])
  end

  def room_out
    if current_room
      session.delete(:room_id)
      @room = nil
    end
  end

end
