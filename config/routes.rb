Rails.application.routes.draw do

  # home_pages
  root                        'home_pages#index'
  get  '/privacy_policy', to: 'home_pages#privacy_policy'

  # users
  get  '/users/search',     to: 'users#index'
  post '/users/create',     to: 'users#create'
  get  '/users/:id',        to: 'users#show'
  post '/image/:id/update', to: 'users#image'

  # names
  get  '/users/:id/name/create', to: 'names#new'
  post '/users/:id/name/create', to: 'names#create'

  # rooms
  get  '/room',            to: 'rooms#index'
  post '/room/:id/create', to: 'rooms#create'
  get  '/room/:id',        to: 'posts#index'

  # posts
  resources :posts

  # tags
  post '/tag/:id/edit', to: 'tags#edit'

  #sessions
  get    '/session', to: 'sessions#new'
  post   '/session', to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
