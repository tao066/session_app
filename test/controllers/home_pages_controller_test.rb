require 'test_helper'

class HomePagesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get home_pages_index_url
    assert_response :success
  end

  test "should get privacy_policy" do
    get home_pages_privacy_policy_url
    assert_response :success
  end

end
