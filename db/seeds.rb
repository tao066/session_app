
categories = ["生徒", "先生"]
categories.each { |category| Category.create!( name: category ) }

tags = ["幼稚園/保育園", "小学校", "中学校", "高校", "大学", "社会人"]
tags.each { |tag| Tag.create!( name: tag ) }

# 自分のアカウント
User.create!(
  email: "tao@tao.com",
  password:              "taotao",
  password_confirmation: "taotao",
  category_id: 1,
  image_name: "student.png",
  tag_id: 5
)
Name.create!(
  last_name: "Tsuchiya",
  first_name: "Kohei",
  user_id: 1
)
current_user = User.find_by(id: 1)

# gimei を用いて自動作成( 99 人 )
m = 2
99.times do |n|
  gimei = Gimei.name
  last_name = gimei.last.romaji
  first_name = gimei.first.romaji
  email = "sample-#{n+1}@sample.com"
  password = "password"
  a = rand(2)+1
  if a == 1
    b = "student.png"
  else
    b = "teacher.png"
  end
  User.create!(
    email: email,
    password:              password,
    password_confirmation: password,
    category_id: a,
    image_name: b,
    tag_id: rand(6)+1
  )
  user = User.find_by(id: m)
  Name.create!(
    last_name: last_name,
    first_name: first_name,
    user_id: m
  )
  Room.create!(
    name: "#{ current_user.name_info } と #{ user.name_info } のセッション"
  )
  room = Room.find_by(id: m-1)
  Contact.create!(user_id: current_user.id, room_id: room.id)
  Contact.create!(user_id: user.id,        room_id: room.id)
  m += 1
end
