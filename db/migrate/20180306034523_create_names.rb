class CreateNames < ActiveRecord::Migration[5.1]
  def change
    create_table :names do |t|
      t.string :last_name
      t.string :first_name
      t.integer :user_id

      t.timestamps
    end
  end
end
